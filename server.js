
var express = require('express');
var users = require('./routes/users');

var app = express();

app.configure(function () {
    app.use(express.logger('dev'));     /* 'default', 'short', 'tiny', 'dev' */
    app.use(express.bodyParser());
});


// app.get('/users', function(req, res) {
//     res.send([{name:'user1'}, {name:'user2'}]);
// });
// app.get('/user/:id', function(req, res) {
//     res.send({id:req.params.id, name: "The Name", description: "description"});
// });

app.post('/users', users.addUser);
app.put('/users/:id', users.updateUser);
app.delete('/users/:id', users.deleteUser);

app.get('/users', users.findAll);
app.get('/users/:id', users.findById);

app.listen(3000);
console.log('Listening on port 3000...');